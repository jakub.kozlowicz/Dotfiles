" vim:fileencoding=utf-8:ft=vim:foldmethod=marker

" PLUGIN MANAGER --------------------------------------------------------- {{{

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" }}}

" PLUGINS ---------------------------------------------------------------- {{{

call plug#begin('~/.vim/plugged')

" Color scheme
Plug 'romgrk/doom-one.vim'

" Better syntax highlighting
Plug 'sheerun/vim-polyglot'

" Add support for yocto/bitbake recipes
Plug 'kergoth/vim-bitbake'

" Status line
Plug 'itchyny/lightline.vim'

" Git signs
Plug 'airblade/vim-gitgutter'

" Easily comment files
Plug 'tpope/vim-commentary'

" Better icons
Plug 'ryanoasis/vim-devicons'

call plug#end()

" }}}

" OPTIONS ---------------------------------------------------------------- {{{

" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

" Fix backspace indent
set backspace=indent,eol,start

" Basic appearance
syntax enable
set number
set autoindent
set smartindent
set cursorline
set signcolumn=yes
set noshowmode
set wrap
set linebreak
set scrolloff=10

" Tabs
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase

" Shell
if exists('$SHELL')
    set shell=$SHELL
else
    set shell=/bin/sh
endif

" Window behavior
set lazyredraw
set splitbelow
set splitright

" Enhance command line
set wildmenu
set wildcharm=<tab>
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" File settings
filetype on
filetype plugin on
filetype indent on
set fileformats=unix,dos,mac
set autoread

" Miscellaneous
set spelllang=en_us,pl
set mouse=a
set clipboard+=unnamedplus
set completeopt=menuone,noinsert,noselect
set shortmess+=c
set updatetime=100
set nobackup
set history=1000

" Nice colors
if (has("termguicolors"))
    set termguicolors
endif
colorscheme doom-one

" Set status line
set laststatus=2

" Customize cursor in different mode
" Reference chart of values:
"   Ps = 0  -> blinking block.
"   Ps = 1  -> blinking block (default).
"   Ps = 2  -> steady block.
"   Ps = 3  -> blinking underline.
"   Ps = 4  -> steady underline.
"   Ps = 5  -> blinking bar (xterm).
"   Ps = 6  -> steady bar (xterm).
let &t_SI = "\e[6 q" " Insert mode
let &t_EI = "\e[2 q" " Normal mode

" }}}

" MAPPINGS --------------------------------------------------------------- {{{

" Set the space as the leader key
nnoremap <space> <nop>
let mapleader = " "

" Window movement mappings
nnoremap <silent> <c-j> <c-w>j
nnoremap <silent> <c-k> <c-w>k
nnoremap <silent> <c-h> <c-w>h
nnoremap <silent> <c-l> <c-w>l

" Resize window
nnoremap <silent> <c-up> <c-w>+
nnoremap <silent> <c-down> <c-w>-
nnoremap <silent> <c-left> <c-w>>
nnoremap <silent> <c-right> <c-w><

" Terminal emulation
nnoremap <silent> <leader>t :terminal<cr>

" Disable highlighting when done searching
nnoremap <silent> <leader>n :nohls<cr>

" Buffer management
nnoremap <leader>, :buffers<cr>:buffer<space>
nnoremap <leader>. :edit<space><tab>
nnoremap <silent> <leader>bk :bd<cr>

" Move visual blocks
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Maintain visual mode after shifting
vmap < <gv
vmap > >gv

" Toggle spell checking
nnoremap <silent> <leader>ss :set spell!<cr>

" }}}

" VIMSCRIPT --------------------------------------------------------------- {{{

if has('autocmd')
    augroup reload_vimrc " Source config file on save
        autocmd!
        autocmd BufWritePost .vimrc source $MYVIMRC | echom 'Reloaded ' . $MYVIMRC | redraw
    augroup END
    augroup trim_spaces " Remove trailing spaces on save
        autocmd!
        autocmd BufWritePre * %s/\s\+$//e
    augroup END
    augroup filetype_vim " Enable folding
        autocmd!
        autocmd FileType vim,zsh,bash,bitbake,conf setlocal foldmethod=marker
    augroup END
endif

" }}}

" PLUGIN SETTINGS -------------------------------------------------------- {{{

" Light line settings
let g:lightline = {
    \ 'colorscheme': 'deus',
    \ }

" }}}
